class CreateUser < ActiveRecord::Migration[5.0]
  def change
    create_table :users do |t|
      t.string :name
      t.string :bio
      t.string :avatar, default: nil

      t.timestamps null: false
    end

    add_index :users, :name, unique: true
  end
end
