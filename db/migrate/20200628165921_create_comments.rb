class CreateComments < ActiveRecord::Migration[5.2]
  def change
    create_table :comments do |t|
      t.text :text, null: false, :in => 1..140
      t.references :user, type: :integer, foreign_key: true
      t.references :post, type: :integer, foreign_key: true

      t.timestamps
    end
  end
end
