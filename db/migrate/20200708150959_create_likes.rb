class CreateLikes < ActiveRecord::Migration[5.2]
  def change
    create_table :likes do |t|
      t.integer :likeable_id
      t.references :user, type: :integer, foreign_key: true
      t.string :likeable_type
    end

    add_index :likes, [:user_id, :likeable_id, :likeable_type], unique: true
  end
end
