Rails.application.routes.draw do
  scope '/', defaults: {format: :json} do
    resources :posts do
      get '/edit', to: 'posts#edit'
    end

    resources :comments, except: [:index]

    resources :users, only: [:show, :update] do
      get '/posts', to: 'posts#user_posts'
      get '/comments', to: 'comments#user_comments'
      post 'upload_avatar', to: 'users#upload_avatar'
      post 'remove_avatar', to: 'users#remove_avatar'
    end

    post '/*likeable_type/:likeable_id/like', to: 'likes#like'
    post '/*likeable_type/:likeable_id/unlike', to: 'likes#unlike'

    devise_for :users,
      path: '',
      path_names: {
        sign_in: 'login',
        sign_out: 'logout',
        registration: 'signup',
      },
      controllers: {
        sessions: 'sessions',
        registrations: 'registrations'
      }
  end
end
