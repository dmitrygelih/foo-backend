json.(@user, :id, :name, :email, :bio)
json.createdAt @user.created_at
json.avatarUrl @user.avatar_url
