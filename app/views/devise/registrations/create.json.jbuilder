json.user do
  json.(current_user, :id, :email, :name)
end
