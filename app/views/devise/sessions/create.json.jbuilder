json.user do
  json.(current_user, :id, :name, :email)
  json.avatarUrl current_user.avatar_url
end