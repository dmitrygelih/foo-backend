if user_signed_in?
  json.user do
    json.(current_user, :id, :email, :avatar)
  end
end