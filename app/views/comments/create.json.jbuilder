json.comment do
  json.partial! 'comments/comment', comment: @comment
  json.partial! 'comments/likes', likesCount: 0, liked: false
end
