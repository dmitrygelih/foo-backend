json.partial! 'comments/base', comment: comment
json.partial! 'comments/author', author: comment.user
