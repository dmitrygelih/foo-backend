json.author do
  json.id author.id
  json.name author.name
  json.avatarUrl author.avatar_url
end
