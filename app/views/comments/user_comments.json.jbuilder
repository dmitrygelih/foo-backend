json.comments @comments do |comment|
  json.partial! 'comments/base', comment: comment
  json.partial! 'comments/likes', likesCount: comment.likes.size, liked: @liked_comment_ids.include?(comment.id)

  json.author do
    json.id comment.user_id
  end
end
