json.post do
  json.partial! 'posts/post', post: @post
  json.partial! 'posts/author', post: @post
  json.partial! 'posts/likes', likesCount: @post.likes_count, liked: @liked_ids.key?('Post') && @liked_ids['Post'].include?(@post.id)
end

json.comments @post.comments do |comment|
  json.partial! 'comments/comment', comment: comment
  json.partial! 'comments/likes', likesCount: comment.likes_count, liked: @liked_ids.key?('Comment') && @liked_ids['Comment'].include?(comment.id)
end
