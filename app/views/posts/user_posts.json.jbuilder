json.posts @posts do |post|
  json.partial! 'posts/post', post: post
  json.partial! 'posts/likes', likesCount: post.likes.size, liked: @liked_posts.include?(post.id)

  json.author do
    json.id post.user_id
  end
end
