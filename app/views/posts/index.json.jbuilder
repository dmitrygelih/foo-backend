json.posts @posts do |post|
  json.partial! 'posts/post', post: post
  json.partial! 'posts/author', post: post
  json.partial! 'posts/likes', likesCount: post.likes_count, liked: @liked_posts_ids.include?(post.id)
end

json.postsCount @posts_count
