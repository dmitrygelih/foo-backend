class CommentsController < ApplicationController
  load_and_authorize_resource only: [:update, :destroy]

  def create
    @comment = Comment.new(comment_params)
    @comment.user = current_user
    @comment.save!
  end

  def update
    @comment.update_attribute(:text, comment_params[:text])
  end

  def user_comments
    @comments = Comment.where(user_id: params.require(:user_id)).includes(:likes).order(created_at: :desc)
    @liked_comment_ids = current_user.likes.where(likeable_type: 'Comment', likeable_id: @comments.ids).pluck(:likeable_id)
  end

  def destroy
    @comment.destroy
  end

  private

  def comment_params
    params.require(:comment).permit(:text, :post_id)
  end

end