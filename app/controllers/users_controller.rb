class UsersController < ApplicationController

  def show
    @user = User.find(params.require(:id))
  end

  def update
    current_user.update!(user_params)
  end

  def remove_avatar
    current_user.remove_avatar!
    current_user.save!
  end

  def upload_avatar
    current_user.update!(avatar: params.require(:avatar))
  end

  private

  def user_params
    params.require(:user).permit(:name, :bio)
  end

end