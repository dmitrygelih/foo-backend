class PostsController < ApplicationController
  before_action :count_posts, only: :index
  prepend_before_action :find_post, only: :edit
  load_and_authorize_resource only: [:edit, :destroy, :update]

  def create
    current_user.posts.create(post_params)
  end

  def index
    return @posts = [] if @posts_count.zero?

    offset = params.require(:offset).to_i
    if @posts_count <= offset
      return render json: { error: 'There are no posts on that page' }, status: :not_found
    end

    @posts = Post.includes(:user).includes(:likes).order(created_at: :desc).limit(params.require(:amount)).offset(offset)
    @liked_posts_ids = current_user.likes.where(likeable_type: 'Post', likeable_id: @posts.ids).pluck(:likeable_id)
  end

  def show
    @post = Post.includes(comments: [:user, :likes]).includes(:user, :likes).find(params.require(:id))

    likes = current_user.likes.where(likeable_id: @post.id, likeable_type: 'Post')
            .or(current_user.likes.where(likeable_id: @post.comments.ids, likeable_type: 'Comment'))

    @liked_ids = Like.group_ids_by_type(likes)
  end

  def update
    @post.update!(post_params)
  end

  def destroy
    @post.destroy
  end

  def user_posts
    @posts = Post.where(user_id: params.require(:user_id)).includes(:likes).order(created_at: :desc)
    @liked_posts = current_user.likes.where(likeable_type: 'Post', likeable_id: @posts.ids).pluck(:likeable_id)
  end

  private

  def post_params
    params.require(:post).permit(:title, :body)
  end

  def find_post
    @post = Post.find(params[:post_id])
  end

  def count_posts
    @posts_count = Post.count
  end

end