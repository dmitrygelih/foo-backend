class RegistrationsController < Devise::RegistrationsController
  before_action :configure_permitted_parameters, if: :devise_controller?
  before_action :check_uniqueness, only: :create

  protected

  def check_uniqueness
    return render_error('Email already has been taken') if User.exists?(email: params[:user][:email])
    return render_error('Username already has been taken') if User.exists?(name: params[:user][:name])
  end

  def configure_permitted_parameters
    devise_parameter_sanitizer.permit(:sign_up, keys: [:name])
  end

  def update_resource(resource, params)
    resource.update_without_password(params)
  end

  private

  def render_error(error)
    render json: { error: error }, status: :unprocessable_entity
  end

end
