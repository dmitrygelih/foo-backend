class LikesController < ApplicationController
  LIKEABLE_TYPES = ['Post', 'Comment']

  def like
    find_likeable_record.likes.create(user: current_user)
  end

  def unlike
    Like.where(
        likeable_id: params.require(:likeable_id),
        likeable_type: get_likeable_type,
        user: current_user
    ).first.destroy
  end

  private

  def get_likeable_type
    type = params.require(:likeable_type).classify

    return type if LIKEABLE_TYPES.any? { |t| t == type }

    render json: { error: 'Wrong like type' }, status: :unprocessable_entity
  end

  def find_likeable_record
    likeable_class = get_likeable_type.safe_constantize
    likeable_class.find(params.require(:likeable_id))
  end

end
