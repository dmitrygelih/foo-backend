class Comment < ApplicationRecord
  belongs_to :user
  belongs_to :post

  include Likeable

  validates :text, presence: true, length: { in: 1..140 }
end