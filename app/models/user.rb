class User < ApplicationRecord
  has_many :posts
  has_many :comments
  has_many :likes

  devise :registerable,
         :database_authenticatable,
         :jwt_authenticatable,
         :validatable,
         jwt_revocation_strategy: JwtDenylist

  mount_base64_uploader :avatar, AvatarUploader

  before_save { email.downcase! }
  validates :name, presence: true, length: { minimum: 3, maximum: 20 }, format: { without: /\s/ }, uniqueness: true
  validates :email, presence: true, format: { with: URI::MailTo::EMAIL_REGEXP }
end