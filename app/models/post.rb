class Post < ApplicationRecord
  belongs_to :user
  has_many :comments, -> { order(created_at: :desc) }, dependent: :destroy

  include Likeable

  validates :title, presence: true, length: { in: 1..30 }
  validates :body, presence: true
end