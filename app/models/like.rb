class Like < ApplicationRecord
  belongs_to :user
  belongs_to :likeable, polymorphic: true

  validates :user_id,
            presence: true,
            uniqueness: {
                scope: [:likeable_id, :likeable_type],
                message: 'User already liked this resource'
            }

  after_commit -> { update_likes_counter_cache(+1) }, on: :create
  after_commit -> { update_likes_counter_cache(-1) }, on: :destroy


  def self.group_ids_by_type(likes)
    result = {}

    likes.each do |l|
      unless result[l.likeable_type]
        result[l.likeable_type] = [l.likeable_id]
        next
      end

      result[l.likeable_type] << l.likeable_id
    end

    result
  end

  private

  def update_likes_counter_cache(value)
    likeable_class = self.likeable_type.safe_constantize
    likeable_class.find(self.likeable_id).increment('likes_count', value).save
  end
end
