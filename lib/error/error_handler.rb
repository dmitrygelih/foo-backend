module Error
  module ErrorHandler

    def self.included(clazz)
      clazz.class_eval do
        rescue_from ActiveRecord::RecordNotFound do |e|
          p '*****************************'
          p '*****************************'
          p '*****************************'
          render json: { error: 'Resource not found' }, status: :not_found
        end

        rescue_from ActiveRecord::RecordNotUnique do |e|
          p '*****************************'
          p '*****************************'
          p '*****************************'
          render json: { error: 'Record not unique' }, status: :unprocessable_entity
        end

        rescue_from CanCan::AccessDenied do |exception|
          render json: { error: exception }, status: :unprocessable_entity
        end
      end
    end

  end
end